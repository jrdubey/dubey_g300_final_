﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{

    private string moveInputAxis = "Vertical";
    private string turnInputAxis = "Horizontal";


    public float turnSpeed = 360f;
    public float moveSpeed = 2f;

    Rigidbody rb;
    float yrot = 0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float moveAxis = Input.GetAxis(moveInputAxis);
        float turnAxis = Input.GetAxis(turnInputAxis);
        ApplyInput(moveAxis, turnAxis);
    
    }

    private void ApplyInput(float moveInput, float turnInput)
    {
        Move(moveInput);
        Turn(turnInput);
    }
    private void Move(float input)
    {
        /* transform.Translate(Vector3.forward * input * moveSpeed)*/
        rb.MovePosition(rb.position + transform.forward * input * moveSpeed);
    }

    private void Turn(float input)
    {
        yrot += input * turnSpeed * Time.deltaTime;
        rb.MoveRotation(Quaternion.Euler(0f,yrot,0f));
    }
}
